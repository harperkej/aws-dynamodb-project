aws dynamodb create-table \
	--table-name project1 \
	--attribute-definitions \
	AttributeName=sku,AttributeType=S \
	--key-schema AttributeName=sku,KeyType=HASH \
	--provisioned-throughput ReadCapacityUnits=5000,WriteCapacityUnits=5000

