const AWS = require('aws-sdk');

const ddb = new AWS.DynamoDB.DocumentClient();

exports.handler = (event, context, callback) => {
    getWatch(event.pathParameters.sku).then((data) => {
        console.log('DATA = ', data);
        if (data && data.Item) watchFoundResponse(data, callback);
        else watchNotFoundResponse(event.pathParameters.sku, callback);
    }).catch((err) => {
        console.error(err);
        errorResponse('Something bad happened!', callback);
    });
};

function getWatch(sku) {
    return ddb.get({
        TableName: 'project1',
        Key: {
            sku: sku,
        },
    }).promise();
}

function errorResponse(errorMessage, callback) {
    callback(null, {
        statusCode: 500,
        body: JSON.stringify({
            Error: errorMessage,
        }),
        headers: {
            'Access-Control-Allow-Origin': '*',
        }
    });
}

function watchFoundResponse(data, callback) {
    callback(null, {
        statusCode: 200,
        body: JSON.stringify({
            sku: data.Item.sku,
            dial_material: data.Item.dial_material,
            case_form: data.Item.case_form,
            case_material: data.Item.case_material,
            movement: data.Item.movement,
            status: data.Item.status,
            year: data.Item.year,
            dial_color: data.Item.dial_color,
            gender: data.Item.gender,
            bracelet_material: data.Item.bracelet_material,
            type: data.Item.type,
        }),
        headers: {
            'Access-Control-Allow-Origin': '*',
        }
    });
}

function watchNotFoundResponse(sku, callback) {
    callback(null, {
        statusCode: 404,
        body: JSON.stringify({
            message: ('No watch found with sku = ' + sku)
        }),
        headers: {
            'Access-Control-Allow-Origin': '*',
        }
    });
}
