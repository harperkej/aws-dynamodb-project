const AWS = require('aws-sdk');
const fs = require('fs');

let configureDynamoDBClient = () => {
    const awsConfig = {
        "region": 'eu-central-1',
        "endpoint": "http://dynamodb.eu-central-1.amazonaws.com",
        "accessKeyId": "AKIAIYGYAPCUBYYS4WYQ",
        "secretAccessKey": "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",
        "correctClockSkew": "true"
    }
    AWS.config.update(awsConfig);
    return new AWS.DynamoDB.DocumentClient();
};

let storeWatchToDynamoDB = (watchDoc, dynamoDBClient) => {
    dynamoDBClient.put(watchDoc, function (error, data) {
        if (error) {
            console.log(error);
        } else {
            console.log('STORED watch. SKU = ', watchDoc.Item.sku);
        }
    });
};

let loadDataToDynamoDb = async () => {
    let delay = 100;
    const jsonFile = fs.readFileSync("watches.json");
    const watches = JSON.parse(jsonFile);
    const awsDynamoDBClient = configureDynamoDBClient();
    watches.forEach(watch => {
        let watchDoc = {
            TableName: "project1",
            Item: watch
        }
        setTimeout(storeWatchToDynamoDB, delay, watchDoc, awsDynamoDBClient);
        delay = delay + 10;
    });
};

loadDataToDynamoDb();