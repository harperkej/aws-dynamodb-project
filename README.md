# AWS DynamoDB project

## Import data to DynamoDB

### Create table
1. The script <b>create-table.sh</b> is configured to create a table called 'project1' with <b>sku</b> as the primary & partition key of type string. This script assumes that the aws cli is configured correctly to work with AWS. <b>After the script is executed make sure that the table is created and wait until the table creation is completed -- check AWS console.</b>
2.  After the table is created, script <b>import-data.sh</b> must be executed to import the data. This script installs nodejs (in case it is not installed) and executes the file <b>index.js</b> which reads the data from <b>watches.json</b> and inserts them in DynamoDB. <b>NOTE</b>: In case you would like to test this with your AWS account check the file <b>index.js</b> and update the credentials with your credentials (method <b>configureDynamoDBClient</b> line <b>4</b>) <b>accessKeyId</b>;<b>secretAccessKey</b>;and possibly <b>region</b> and <b>endpoint</b> in case you're running the DynamoDB in anothe region.

### Test the API

The API can be tested in the following link:

https://w9ckammi2b.execute-api.eu-central-1.amazonaws.com/prod/watches/v3/get/{sku}


<b>NOTE:</b> Replace path parameter <b>{sku}</b> with a real key.

The code of the lamba function can be found at the following file: <b>getWatchBySKu.js</b>
